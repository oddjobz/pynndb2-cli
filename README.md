# PyNNDB2-CLI

Command line shell for the PyNNDB2 database library.

* Docs [pynndb2_ docs are here]

.. _pynndb2: https://pynndb.madpenguin.uk/
