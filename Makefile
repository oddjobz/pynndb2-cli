uploadtest:
	rm -f dist/*
	pipenv lock --requirements > requirements.txt
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload --repository pypitest dist/*
	rm requirements.txt

upload:
	rm -f dist/*
	pipenv lock --requirements > requirements.txt
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload --repository pypicli dist/*
	rm requirements.txt

install:
	pipenv lock --requirements > requirements.txt
	python3 setup.py install
	rm requirements.txt
